---
title: "Azure policies"
date: 2019-02-09T19:58:26+02:00
description : ""
#draft: false
tags: ["azure"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

 # Assign Policy

* Powershell
```powershell
# Assign policy to recource group
$rg = Get-AzResourceGroup -name RG1
$policy_def = Get-AzPolicyDefinition | ?{ $_.Properties.Displayname -eq "Audit VMs that do not use managed disks" }
New-AzPolicyAssignment -Name "Check for managed disks" -DisplayName "Check for managed disks" -Scope $rg.ResourceId -PolicyDefinition $policy_def
```
* Bash
```bash

```