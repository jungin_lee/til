---
title: "Manage Azure resources and groups"
date: 2019-02-01T22:08:50+02:00
description : ""
#draft: false
tags: ["Azure Resource Groups"] #[ "Development", "Go", "Powershell", "Blogging" ]
---
Azure Powershell RM is replaced with powershell az. commabds will be New-AzResourceGroup

# Resource Groups
## Get resource groups
* powershell
```powershell
# Get all resourcegroups
Get-AzResourceGroup
# Get specific resourcegroup
Get-AzResourceGroup -ResourceGroupName NetworkWatcherRG
# Get resources in resource group
Get-AzResource -ResourceGroupName NetworkWatcherRG
```

## Create resource groups
* Powershell
```powershell 
New-AzureRmResourceGroup -name "resource-groep-name" -Location "West Europe"
```
* Bash
```shell
az group create --name resource-groep-name --location "West Europe"
```

# Azure Resource Tags
Can be used on any Azure resource or group

* Powershell
```powershell
# Get tags of resource
(Get-AzResource -ResourceName Ubuntu1).Tags
# Get all resources with specific tag
Get-AzResource -TagName Dept
#Get resources by tag and value
Get-AzResource -Tag @{Dept="IT"}
#get all used tags
Get-AzTag

# Add one tag
Set-AzResourceGroup -Name plaz-prod1-rg -Tag @{ Dept="Research"; Owner="FreekBos" }

# Add extra resourse group
$resource = Get-AzResource -ResourceName Ubuntu1
$tags = $resource.Tags
$tags += @{Dept="IT"; LifeCyclePhas="Testing"}
Set-AzResource -ResourceId $resource.Id -Tag $tags -Force

# Erase tags
Set-AzResourceGroup -Tag @{} -Name plaz-prod1-rg
```

* Bash
```bash
# Get tags of resource
az tag list
# get tags of a resource
az group show -n NetworkWatcherRG --query tags

# Add tags to a resource (via tag)
az resource tag --tags LifeCyclePhase=Dev Region=Central -g cloud-shell-storage-westeurope -n Ubuntu1 --resource-type "Microsoft.Compute/virtualMachines"

# Add resource tag (via update)
az group update -n plaz-app1-rg --set tags.Owner=BradCocco tags.Dept=IT

# Add extra resourse group
az group update -n plaz-app1-rg --set tags.CostCenter=Store
```

# Locks
set locks on recources
* Powershell
```powershell
# Set lock on resourcegroep for no deletion
New-AzureRmResourceLock -LockName prod1NoDelete -LockLevel CanNotDelete -ResourceGroupName plaz-prod1-rg

# Get resource locks active
Get-AzureRmResourceLock -ResourceGroupName plaz-prod1-rg

# Delete resource lock
remove-AzureRmResourceLock -ResourceGroupName plaz-prod1-rg -LockName prod1NoDelete
# Or via ID
$lockId = (Get-AzureRmResourceLock -ResourceGroupName plaz-prod1-rg).LockID
Remove-AzureRmResourceLock -LockId $lockid
```
* Bash
```bash
#Create resource lock
az lock create --name Lockapp1 --lock-type ReadOnly --resource-group plaz-app1-rg
```
# Access Controll (IAM)
* Powershell
```powershell
# Get IAM of a resource
Get-AzureRmRoleAssignment -ResourceGroupName plaz-prod1-rg

# Assign role to a user
New-AzureRmRoleAssignment -SignInName user@dom.onmicrosoft.com -RoleDefinitionName "Reader" -ResourceGroupName "plaz-dev-rg"

# Assign role to a group (with group id)
Get-AzureADGroup -SearchString "Research"
New-AzureRmRoleAssignment -ObjectId $groupID -RoleDefinition "Owner" -ResourceGroupName "plaz-dev-rg"

```

* Bash
```bash
# Get rousource groep role assignments
az role assignment list --resource-group plaz-dev-rg

# New role assignment for a user
az role assignment create --role Owner --assignee user@dom.onmicrosoft.com --resource-group plaz-app1-rg

# New role assignment for a group
az ad group list
az role assignment create --role "Virtual Machine Contributor" --assignee-object-id $groupID --resource-group plaz-app1-rg
```

# Azure Resource Policy
* Powershell
```powershell
$rg = Get-AzureRmResourceGroup -Name 'plaz-prod1-rg'
$definition = New-AzureRmPolicyDefinition -Name "allowed-locations" -DisplayName "Allowed locations" -description "This policy enables you to restrict the locations your organization can specify when deploying resources. Use to enforce your geo-compliance requirements." -Policy 'https://raw.githubusercontent.com/Azure/azure-policy/master/samples/built-in-policy/allowed-locations/azurepolicy.rules.json' -Parameter 'https://raw.githubusercontent.com/Azure/azure-policy/master/samples/built-in-policy/allowed-locations/azurepolicy.parameters.json' -Mode All
$definition
$assignment = New-AzureRMPolicyAssignment -Name "only East US" -Scope $rg.ResourceId  -listOfAllowedLocations "East US" -PolicyDefinition $definition
$assignment
```

# Move resources to other resource group
* Powershell
```powershell
$resource = Get-AzureRmResource -ResourceGroupName plaz-net2-rg -ResourceName VNet1
Move-AzureRmResource -DestinationResourceGroupName plaz-net-rg -ResourceId $resource.ResourceId
```
* Bash
```bash
$resource=(az resource show -g plaz-net-rg -n vnet1 --resource-type "Microsoft.Network/virtualNetworks" --query id --output tsv)
az resource move --destination-group plaz-net2-rg --id $resource
```

# Delete a resource group
* Powershell
```powershell
Remove-AzureRmResourceGroup -name plaz-vm01-rg
```
* Bash
```bash
az group delete -n plaz-prod1-rg
```