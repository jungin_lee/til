---
title: "Selecting and Sorting Objects"
date: 2018-01-29T12:29:53+01:00
description : ""
#draft: false
tags: ["Powershell"] #[ "Development", "Go", "Powershell", "Blogging" ]
weight: 4
---
## Select properties to show
Let you select object (the selected objects will be the only objects you can use).
```Powershell
Get-Process | Select-Object -Property Name,ID,VM,PM
Get-Process | Select Name,ID,VM,PM

Get-ChildItem | Select-Object -Property Name,Directory
Get-ChildItem | Select -Property Name,Directory
```

## Sorting
```Powershell
Get-Process | Sort-Object -property VM -descending
Get-Process | Sort VM -desc
```


### Select last or first items
```Powershell
Get-Process | Select-Object -First 10
Get-Process | Select-Object -Last 10
```


### Select ExpandProperty
_**need more information on this**_
```Powershell
Get-Service winmgmt | select -ExpandProperty DependentServices
(get-service winmgmt).RequiredServices
```


### calculated properties

To modify the output with calculated properties requires a hashtable with a Name and an Expression key  `@{ Name = ''; Expression = {}}` . The name key is a variable, Expression a script block.
```Powershell

Get-ChildItem | Select-Object -Property Name,Directory,@{ Name = 'Format'; Expression ={$_.Extension}}
Get-Process | Format-Table ID,Name,@{N="Memory (MB)";E={$_.VM / 1MB}}

dir | ft Name,@{n="size";e={"{0:0.00}M" -f ($_.Length / 1MB)}}

Get-WmiObject -Class WIN32_volume -Filter 'drivetype = 3' | Select-Object -Property PScomputerName, DriveLetter, Label, @{l='FreeSpace(GB)';e={'{0:N2}' -f ($_.freespace/1GB)} }
(Get-CimInstance -ClassName Win32_PhysicalMemory | Measure-Object -Property capacity -Sum).Sum | Select-Object @{N='Memory (GB)';E={$_ / 1GB}}
```

## Rename object properties with custom properties
```Powershell
import-csv .\newusers.csv |
select-object -property *,
@{name='samAccountName';expression={$_.login}},
@{label='Name';expression={$_.login}},
@{n='Department';e={$_.Dept}}
```
