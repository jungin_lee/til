---
title: "Manage Shares with Powershell"
date: 2018-10-26T11:50:33+02:00
description : ""
#draft: false
tags: ["powershellshares"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## View shares
Git list of shares
```powershell
Get-SmbShare
```

## current sessions
Get current sessions
```powershell
Get-SmbSession
```

## Open files
```powershell
Get-SmbOpenFile
# or
Get-SmbOpenFile | Out-GridView
# or
Get-SmbOpenFile | Out-GridView -OutputMode Multiple | Close-SmbOpenFile -Force
```