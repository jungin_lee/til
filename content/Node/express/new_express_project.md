---
title: "New Express Project"
date: 2018-12-01T21:45:18+02:00
description : ""
#draft: false
tags: ["express"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

# Best way to start new Express app

## create express project
Go to the project folder in the console and run `express -v hbs`. This will create a package.json with start script and depencies. It will laso creates folders for routes, views(handlebars), public and bin. run `npm install` to install all the modules.

Initialize Git with `git init`. Create a gitignore file for node with `gitignore node`. Add all the files `git add .` and do a commit `git commit -m 'First commit'`