---
title: "Node"
date: 2018-05-28T16:33:52+02:00
description : ""
#draft: false
tags: ["node"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Beginning with Node
* [ ] https://nodeschool.io/
* [ ] https://www.rithmschool.com/courses/node-express-fundamentals
* [ ] https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs
* [ ] https://glitch.com/edit/#!/node-beginner
* [ ] https://github.com/workshopper/learnyounode
* [ ] https://github.com/therebelrobot/awesome-workshopper#readme
* [ ] https://github.com/maxogden/art-of-node
* [ ] https://blog.risingstack.com/node-hero-tutorial-getting-started-with-node-js/
* [ ] https://github.com/substack/stream-handbook
* [ ] http://nicholasjohnson.com/node/course/exercises/
* [ ] https://github.com/goldbergyoni/nodebestpractices
* [ ] http://thenodeway.io/
* [ ] https://github.com/azat-co/you-dont-know-node
* [ ] https://frameworkless.js.org/course

## Video tutorials
* https://www.youtube.com/playlist?list=PL4cUxeGkcC9gcy9lrvMJ75z9maRw4byYp - Node JS Tutorial for Beginners
* https://www.youtube.com/playlist?list=PL55RiY5tL51oGJorjEgl6NVeDbx_fO5jR - Node.js Basics
* https://www.youtube.com/playlist?list=PLillGF-RfqbYRpji8t4SxUkMxfowG4Kqp - Node.js & Express From Scratch
* https://www.youtube.com/playlist?list=PLGLfVvz_LVvSpxyVx5XcprEgvhJ1BzruD - NodeJS Tutorial
* https://www.youtube.com/playlist?list=PLx9YLpPjw0mgbAMF1T4pZY_4tLee-uOyC - earn Nodejs with 10 projects

## Node things to do and check
* https://blog.risingstack.com/node-hero-node-js-authentication-passport-js/


* https://blog.intelligentbee.com/2017/06/16/node-js-guide-list-top-frameworks-tools-ides/
