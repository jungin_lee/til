---
title: "Touch Typing"
date: 2019-12-19T22:41:47+02:00
description : ""
#draft: false
#tags:
#- touchTyping
#- Development
#- Go
#- Powershell
#- Blogging
---

* [Keybr - typing speed test](https://www.keybr.com/)
* [Typing club - online game like test](https://www.typingclub.com)
* [Typing study](https://www.typingstudy.com)
* [Typing.com](https://www.typing.com/)
