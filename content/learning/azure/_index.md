---
title: "Azure"
description : ""
#tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
#pre: "<i class='fa fa-github'></i> "
---

{{% children depth="999" %}}

* [X] [Learn Azure in a Month of Lunches](https://azure.microsoft.com/en-us/resources/learn-azure-in-a-month-of-lunches/en-us/?OCID=AID681541_aff_7593_1243925&epi=lw9MynSeamY-tJ8X3Dhei4FRaOypZLPDXQ&irgwc=1&ranEAID=lw9MynSeamY&ranMID=24542&ranSiteID=lw9MynSeamY-tJ8X3Dhei4FRaOypZLPDXQ&tduid=%28ir__egl) (free e-book)^(Run the azure CLI from windows linux subsystem or a full linux install to learn some linux!)

## AZ-100 

* [AZ-100 curriculum](https://www.microsoft.com/en-us/learning/exam-AZ-100.aspx)
* [https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-100.1+2019\_T1/about](https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-100.1+2019_T1/about) AZ-100 courseware
* [https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-100.2+2019\_T1/about](https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-100.2+2019_T1/about)
* [https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-100.3+2019\_T1/about](https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-100.3+2019_T1/about)
* [https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-100.4+2019\_T1/about](https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-100.4+2019_T1/about)
* [https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-100.5+2019\_T1/about](https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-100.5+2019_T1/about)
* [Hands-onlabs for the above course](https://github.com/MicrosoftLearning/AZ-100-MicrosoftAzureInfrastructureDeployment)
* [https://docs.microsoft.com/en-us/learn/browse/](https://docs.microsoft.com/en-us/learn/browse/) More hands-onlabs
* [https://www.learnondemandsystems.com/blog-azure-series/](https://www.learnondemandsystems.com/blog-azure-series/) Challange labs to test yourself
* [https://courses.skylinesacademy.com/p/az-100](https://courses.skylinesacademy.com/p/az-100) Video course i used as extra
* [https://linuxacademy.com/azure/training/course/name/microsoft-azure-infrastructure-and-deployment-exam-az-100](https://linuxacademy.com/azure/training/course/name/microsoft-azure-infrastructure-and-deployment-exam-az-100) Video course that might be worth checking
* [https://www.pluralsight.com/partners/microsoft/azure](https://www.pluralsight.com/partners/microsoft/azure) (free azure courses on Pluralsight) i did not like the flow

## AZ-101

* https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-101.1+2019_T1/course/
* https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-101.2+2019_T1/course/
* https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-101.3+2019_T1/course/
* https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-101.4+2019_T1/course/

## AZ-103

* https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-103.1+2019_T2/course/
* https://github.com/MicrosoftLearning/AZ-103-MicrosoftAzureAdministrator


Some other usefull links:

* [https://github.com/BMichaelJ/Microsoft-Azure-Exams-Tips/blob/master/AZ-100Links.md](https://github.com/BMichaelJ/Microsoft-Azure-Exams-Tips/blob/master/AZ-100Links.md)
* [https://github.com/BMichaelJ/Azure-links/blob/master/README.md#Training](https://github.com/BMichaelJ/Azure-links/blob/master/README.md#Training)
* [https://msandbu.org/study-guide-for-azure-certifications-az-100-az-101/](https://msandbu.org/study-guide-for-azure-certifications-az-100-az-101/)
* [https://www.iamondemand.com/blog/preparing-for-the-az-100-exam-heres-a-cheat-sheet/](https://www.iamondemand.com/blog/preparing-for-the-az-100-exam-heres-a-cheat-sheet/)
* [https://www.skylinesacademy.com/azure-study-resources/](https://www.skylinesacademy.com/azure-study-resources/)
* [https://jarnobaselier.nl/microsoft-az-103-cheatcheet/](https://jarnobaselier.nl/microsoft-az-103-cheatcheet/)

Keep in mind Microsoft changing to the new [Powershell Az Module](https://docs.microsoft.com/en-us/powershell/azure/new-azureps-module-az?view=azps-1.5.0).