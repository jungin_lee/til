---
title: "Azure VM Powershell"
date: 2019-05-29T11:44:26+02:00
description : ""
#draft: false
tags: ["azure", "powershell"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

### Management commands

Get status of a machine

```powershell
Get-AzVM

get-azvm -status

get-azvm -name SimpleWinVM -ResourceGroupName deploymentgroup
```

Update VM settings

```powershell
$ResourceGroupName = "deploymentgroup"
$vm = Get-AzVM  -Name SimpleWinVM -ResourceGroupName $ResourceGroupName
$vm.HardwareProfile.vmSize = "Standard_A3"

Update-AzVM -ResourceGroupName $ResourceGroupName  -VM $vm
```

### Creating machines

See *Azure Virtual Machine create with Powershell*.

```powershell
# New VM simple
New-AzVM -name azvm1 -ResourceGroupName azgroup1
```

[Azure Virtual Machine Sample Scripts](https://docs.microsoft.com/en-us/azure/virtual-machines/windows/powershell-samples)
