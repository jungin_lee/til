---
title: "Azure VM Availability"
date: 2019-06-03T11:44:04+02:00
description : ""
#draft: false
tags: ["azure"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

See the [Regions and availability](https://docs.microsoft.com/en-us/azure/virtual-machines/windows/regions-and-availability#availability-sets) page at Microsoft.

## Maintenance and Downtime

There are three scenarios that can lead to virtual machine in Azure being impacted.

* **Unplanned Hardware Maintenance** - Occurs when the Azure platform predicts that the hardware or a platform component is about to fail. This will issue and hardware maintenance event. Azure uses [Live Migration](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/maintenance-and-updates#live-migration) technology to move the VM to a healthy physical machine. Memory, open files and network connections are still active after a Live Migration. The performance may get reduced before and after the migration.
* **Unexpected Downtime** - Is when the hardware or infrastructure for the VM fails unexpectedly. This can include network, disk or rack (power) failures. When the Azure platform detects a failure the VM will migrate to a healthy physical machine. During the healing procedure the VM will be down and do a new boot. In some cases you will lose the temporary disk.
* **Planned Maintenance** - Are periodic updates made by Microsoft to the underlying platform. Most updates are without any impact on the VM. There are various [maintenance scenarios](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/maintenance-and-updates), some will pause a machine, reboot a machine or perform a live migration.

Microsoft does not automatically update your VMs OS or software. The underlying software host and hardware are periodically patched.
Also see the [Azure VM Availability article](https://docs.microsoft.com/bs-cyrl-ba/azure/virtual-machines/windows/manage-availability).

## Availability Sets

An [Availability Set](https://docs.microsoft.com/nl-nl/azure/virtual-machines/windows/manage-availability#configure-multiple-virtual-machines-in-an-availability-set-for-redundancy) is a logical group of virtual servers to eliminate a single point of failure or upgraded at the same time. The VM machines in a availability set should be identical and perform the same functionality.

An Availability Set consists of the following two:

* **Fault domains** - VMs placed in Fault domain will ensure they will be spread across different physical servers, compute racks, storage units, and network switches.
* **Update domains** - If VMs are separated by an Update domain will not get updated at the same time. During planned maintenance, only one update domain is rebooted.

Keep the following rules in mind:

* Configure multiple virtual machines in an Availability Set.
* Configure Each Application tier in a separate Availability Set.
* Combine Load Balancers with Availability Sets.
* Use managed Disks with Availability Sets.

## Availability zones

...

### Service Level Agreements

The Azure SLA is based on the availability.

* For two VMs in a availability set Microsoft will guarantee a connectivity of at least 99.95% of the time.
* For Two VMs or more deployed across two or more Availability Zones in the same Azure region, Microsoft will guarantee a connectivity of at least 99.99% of the time.
* For any Single Instance Virtual Machine using premium storage for all disks, we guarantee you will have Virtual Machine Connectivity of at least 99.9%.

See the [Azure SLA](https://azure.microsoft.com/en-us/support/legal/sla/summary/) for more details and return of credits.
