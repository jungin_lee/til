---
title: "Azure VM Scaling"
date: 2019-06-04T08:22:24+02:00
description : ""
#draft: false
tags: ["azure"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

!!!! Scale sets and availability update domain controleren / testen. Of [Placement groups](https://docs.microsoft.com/en-us/azure/virtual-machine-scale-sets/virtual-machine-scale-sets-placement-groups) and [this](https://coding-stream-of-consciousness.com/2019/02/27/azure-scale-set-vs-availability-set/)

## Scale Sets

Azure virtual machine scale sets let you create and manage a group of identical, load balanced VM instances. The number of machines can increase automatically, by hand or on a schedule.

* All VMs are created from the same OS image and provisioning configuration. In this way you can deploy hundreds of identical VMs.
* Scale sets support the use of Azure load balancer for basic layer-4 traffic distribution and Azure Application Gateway for advanced layer-7 traffic distribution and SSL termination.
* Scale sets are used to run multiple instances of a application. If one instance has a problem the other one will take over. The VMs can be spread over different [Availability Zones](https://docs.microsoft.com/en-us/azure/virtual-machine-scale-sets/virtual-machine-scale-sets-use-availability-zones).
* Machines in a scale set are placed over [5 fault domains and 5 update domains](https://docs.microsoft.com/en-us/azure/virtual-machine-scale-sets/virtual-machine-scale-sets-faq) automatically.
* When the demand is higher you can use [Autoscale](https://docs.microsoft.com/en-us/azure/virtual-machine-scale-sets/virtual-machine-scale-sets-autoscale-overview) based on different rules.
* Scale sets support up to 1000 VMs with normal images. With custom images the limit is 300 instances.

## Autoscale

Automatically adjust the number of instances based on a set of rules. When the performance on one server is to high, Azure can scale up the capacity of the Scale set.

* Scale Out - If the application demand increases the load on the current instances, and the increased load is consistent. Autoscale can scale up the number of instances.
* Scale In - When the load is low for a long period of time. Autoscale can decrease the number of machines. The scale-in action reduces the cost.
* Scheduled event - Schedule events to automatically increase or decrease the capacity of your scale set at fixed times.

See the [best practices for Autoscale](https://docs.microsoft.com/en-us/azure/azure-monitor/platform/autoscale-best-practices).
