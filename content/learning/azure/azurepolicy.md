---
title: "Azure Policy and Compliance"
date: 2019-06-12T11:48:08+02:00
description : ""
#draft: false
tags: ["azurepolicy"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Terminology

* Policy Definitions
* Initiative Definitions

## Azure Policy service

Azure Policy allows you to create, assign and manage policies. These policies enforce rules over your resources, so those resources stay compliant with your standards. Azure does this by running evaluations of your resources and scanning for those not compliant.

* **Enforcement and compliance** - Turn on built-in policies or build custom ones for all resource types. Real time policy evaluation and enforcement. Periodic and on-demand compliance evaluation.

* **Apply policies at scale** - Apply policies to a Management Group with control across your entire organization. Apply multiple policies and aggregate policy states with policy initiative. Define an exclusion scope.

* **Remediation** - Real time remediation, and remediation on existing resources.

Azure policy will be important if your team runs an environment with multiple teams, subscriptions and you need to standarize how the cloud is configured. Or you need te manage  cost control, security, and consistency compliance.

## Implementing Azure Policy

To implement Azure policies, follow these steps:

* **Browse Policy Definition** - A Policy Definition expresses what to evaluate and what actions to take. Every policy definition has conditions under which it is enforced. And, it has an accompanying effect that takes place if the conditions are met. For example, you could prevent VMs from being deployed if they are exposed to a public IP address.
* **Create Initiative Definitions** - An initiative definition is a set of Policy Definitions to help track your compliance state for a larger goal. For example, ensuring a branch office is compliant.
* **Scope the Initiative Definition** - You can limit the scope of the Initiative Definition to Management Groups, Subscriptions, or Resource Groups.
* **View Policy Evaluation results** - Once an Initiative Definition is assigned, you can evaluate the state of compliance for all your resources. Individual resources, resource groups, and subscriptions within a scope can be exempted from having policy rules affect it. Exclusions are handled individually for each assignment.

## Policy Definitions

Here is a small list of available policy definitions:

* **Require SQL Server 12.0**: Validates that all SQL servers use version 12.0. Its effect is to deny all servers that don't meet these criteria.
* **Allowed Storage Account SKUs**: Determines if a storage account being deployed is within a set of SKU sizes. Its effect is to deny all storage accounts that don't adhere to the set of defined SKU sizes.
* **Allowed Resource Type**: Defines the resource types that you can deploy. Its effect is to deny all resources that aren't part of this defined list.
* **Allowed Locations**: Restricts the available locations for new resources. Its effect is used to enforce your geo-compliance requirements.
* **Allowed Virtual Machine SKUs**: Specifies a set of virtual machine SKUs that you can deploy.
* **Apply tag and its default value**: Applies a required tag and its default value if it's not specified by the deploy request.
* **Enforce tag and its value**: Enforces a required tag and its value to a resource.
* **Not allowed resource types**: Prevents a list of resource types from being deployed.

If the policy you need is not in the list, you can create policy definition in a [specific JSON format](https://docs.microsoft.com/en-us/azure/governance/policy/concepts/definition-structure).

Currently, an Initiative Definition can have up to 100 policies.

## Initiative Definitions

It is recommended to always create a initiative definition, even if you include one policy Definition. 

## Determine Compliance

Policy evaluation happens about once an hour, which means that if you make changes to your policy definition and create a policy assignment then it will be re-evaluated over your resources within the hour.

## Resources

* [Azure Policies](https://docs.microsoft.com/en-us/azure/governance/policy/overview) and Compliance
* [Azure Policy definition structure](https://docs.microsoft.com/en-us/azure/governance/policy/concepts/definition-structure)
