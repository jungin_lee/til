---
title: "Azure Basics"
date: 2019-05-01T15:20:25+02:00
description : ""
#draft: false
tags: ["azure, cloud"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Azure Region infrastructure

* Region - One or more linked datacenters
* Geography - Two or more regios in a geography or compliance erea.
* Region Pairs - Region pair in Geography more tgen 300 miles away from each other. Only one region in a pair will get updates in a time.

## Azure availability

* Fault Domain - Seperate server rack of power and network in same datacenter.
* Update Domain - Server in the same datacenter on a other update schedule.
* Availability set - A config set of the fault and update domain.
* Availability Zone - Two or three datacenters in the same region. A combination of a fault and update domain. The platform will only update one zone at a time. The datacenters are on seperate power systems and spread over one region with low latency connections.

## Azure SLA

Service Credit - Return percentage if SLA is not met.

## Azure components

### Azure Compute

* Virtual machines -
* Containers -
* Azure Kubernetes -
* Azure App Service - Dedicated PaaS web HTTP servers.
* Serverless Computing -
